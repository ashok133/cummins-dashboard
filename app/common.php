<?php
// Change the working directory to this file.
chdir(__DIR__);
set_include_path (__DIR__);
if ($_SERVER['REQUEST_METHOD'] == 'POST' && stripos($_SERVER['CONTENT_TYPE'], 'application/json') !== false ) {
  $_POST = json_decode(file_get_contents('php://input'), true);
}
require 'environment.php';
/** MODELS **/
require 'models/Comment.php';
require 'models/Client.php';
require 'models/Sensor.php';
require 'models/Site.php';
require 'models/Turbine.php';
require 'models/SensorDeployed.php';
require 'models/SensorTimeSeries.php';
require 'models/TurbineDeployed.php';
require 'models/ClientNotes.php';

require 'models/Engines.php';
require 'models/Client_data.php';
require 'models/Parts_data.php';
require 'models/Supplier_data.php';
require 'models/Client_purchase_data.php';
