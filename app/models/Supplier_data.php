<?php

//require 'printer.php';

class Supplier_data{

  public $supplierID;
  public $supplierName;
  public $addressCity;
  public $addressState;
  public $phone;

  public function __construct($data){
    $this->supplierID = isset($data['supplierID']) ? intval($data['supplierID']):null;
    $this->supplierName=$data['supplierName'];
    $this->addressCity = $data['addressCity'];
    $this->addressState = $data['addressState'];
    $this->phone = $data['phone'];
  }

  public static function fetchAll(){
    $db= new PDO(DB_SERVER,DB_USER,DB_PW);
    $sql= 'SELECT * from SUPPLIER_DATA';
    $statement=$db->prepare($sql);
    $success=$statement->execute();
    $arr=[];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
      $temp =  new Supplier_data($row);
      array_push($arr, $temp);
    }
    return $arr;
  }

  // public function create() {
  //   $db = new PDO(DB_SERVER, DB_USER, DB_PW);
  //   $sql = 'INSERT COMMENT_PHP(comment) VALUES (?)';
  //   $statement = $db->prepare($sql);
  //   $success = $statement->execute([
  //     $this->comment
  //   ]);
  //   $this->id = $db->lastInsertId();
  //   $temp = array (
  //         "id"=>$this->id,
  //         "comment"=>$this->comment
  //       );
  // }
}
