<?php

//require 'printer.php';

class Client_data{

  public $clientID;
  public $name;
  public $addressCity;
  public $addressState;
  public $phone;
  public $yearsSinceclient;
  public $lastPurchaseID;

  public function __construct($data){
    $this->clientID = isset($data['clientID']) ? intval($data['clientID']):null;
    $this->name=$data['name'];
    $this->addressCity = $data['addressCity'];
    $this->addressState = $data['addressState'];
    $this->phone = $data['phone'];
    $this->yearsSinceClient = $data['yearsSinceClient'];
    $this->lastPurchaseID = $data['lastPurchaseID'];
  }

  public static function fetchAll(){
    $db= new PDO(DB_SERVER,DB_USER,DB_PW);
    $sql= 'SELECT * from CLIENT_DATA';
    $statement=$db->prepare($sql);
    $success=$statement->execute();
    $arr=[];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
      $temp =  new Client_data($row);
      array_push($arr, $temp);
    }
    return $arr;
  }

  // public function create() {
  //   $db = new PDO(DB_SERVER, DB_USER, DB_PW);
  //   $sql = 'INSERT COMMENT_PHP(comment) VALUES (?)';
  //   $statement = $db->prepare($sql);
  //   $success = $statement->execute([
  //     $this->comment
  //   ]);
  //   $this->id = $db->lastInsertId();
  //   $temp = array (
  //         "id"=>$this->id,
  //         "comment"=>$this->comment
  //       );
  // }
}
