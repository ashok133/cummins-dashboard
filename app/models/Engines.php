<?php

//require 'printer.php';

class Engines{

  public $engineID;
  public $engineName;
  public $capacity;
  public $numOfParts;
  public $partIDs;
  public $retailPrice;

  public function __construct($data){
    $this->engineID = isset($data['engineID']) ? intval($data['engineID']):null;
    $this->engineName=$data['engineName'];
    $this->capacity = $data['capacity'];
    $this->numOfParts = $data['numOfParts'];
    $this->partIDs = $data['partIDs'];
    $this->retailPrice = $data['retailPrice'];
  }

  public static function fetchAll(){
    $db= new PDO(DB_SERVER,DB_USER,DB_PW);
    $sql= 'SELECT * from ENGINES';
    $statement=$db->prepare($sql);
    $success=$statement->execute();
    $arr=[];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
      $temp =  new Engines($row);
      array_push($arr, $temp);
    }
    return $arr;
  }

  // public function create() {
  //   $db = new PDO(DB_SERVER, DB_USER, DB_PW);
  //   $sql = 'INSERT COMMENT_PHP(comment) VALUES (?)';
  //   $statement = $db->prepare($sql);
  //   $success = $statement->execute([
  //     $this->comment
  //   ]);
  //   $this->id = $db->lastInsertId();
  //   $temp = array (
  //         "id"=>$this->id,
  //         "comment"=>$this->comment
  //       );
  // }
}
