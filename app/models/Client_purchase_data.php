<?php

//require 'printer.php';

class Client_purchase_data{

  public $purchaseID;
  public $clientID;
  public $dateOfPurchase;
  public $productOrdered;
  public $quantity;
  public $totalAmount;
  public $status;

  public function __construct($data){
    $this->purchaseID = isset($data['purchaseID']) ? intval($data['purchaseID']):null;
    $this->clientID=$data['clientID'];
    $this->dateOfPurchase = $data['dateOfPurchase'];
    $this->productOrdered = $data['productOrdered'];
    $this->quantity = $data['quantity'];
    $this->totalAmount = $data['totalAmount'];
    $this->status = $data['status'];
  }

  public static function fetchAll(){
    $db= new PDO(DB_SERVER,DB_USER,DB_PW);
    $sql= 'SELECT * from CLIENT_PURCHASE_DATA';
    $statement=$db->prepare($sql);
    $success=$statement->execute();
    $arr=[];
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
      $temp =  new Client_purchase_data($row);
      array_push($arr, $temp);
    }
    return $arr;
  }

  // public function create() {
  //   $db = new PDO(DB_SERVER, DB_USER, DB_PW);
  //   $sql = 'INSERT COMMENT_PHP(comment) VALUES (?)';
  //   $statement = $db->prepare($sql);
  //   $success = $statement->execute([
  //     $this->comment
  //   ]);
  //   $this->id = $db->lastInsertId();
  //   $temp = array (
  //         "id"=>$this->id,
  //         "comment"=>$this->comment
  //       );
  // }
}
