var cumminsApp = new Vue({
	el: '#aggregatorDiv',
	data : {
		pathLats : [],
		pathLngs : [],
		engineTimeSeries:[],
		serviceLog: [],
		faultLogs: [],
		faultLogNumber: 0,
		detectedProblems: [],
		suggestedMeasures: [
			'Immediate: Check the pressure valve',
			'Service: Send fault log and service request to CSX team'
		],
		avgVolEff: 0,
		avgFuelAir: 0,
		avgThermalEff: 0,
		avgPeakPressure: 0,
		clientDetails: [],
		clientPurchaseDetails: [],
		images: {
				he: "images/he.jpg",
				pte: "images/pte.jpg"
			},
		clientInfo: [
			{
				clientId: '',
        clientName: '',
        clientDescription: '',
        gicsSector: '',
        gicsSubIndustry: '',
        headquarters: ''
			}
		],
		sites: [
			{
				siteId: '',
				clientId: '',
				siteName: '',
				siteDescription: '',
				primaryContact: '',
				capacity: ''
			}
		],
			sensorTimeSeries: [
				{
	        sensorDeployedId: '',
	        dataCollectedDate: '',
	        output: '',
	        heartRate: '',
	        compressorEfficiency: '',
	        availability: '',
	        reliability: '',
	        firedHours: '',
	        trips: '',
	        starts: ''
    		}
			],
			turbineDeployedSpecific:[
			{
					turbineDeployedId: '',
					turbineId: '',
					siteId: '',
					serialNumber: '',
					deployedDate: '',
					totalFiredHours: '',
					totalStarts: '',
					lastPlannedOutageDate: '',
					lastUnplannedOutageDate: ''
			}
		],
			turbineDeployed: [
	    {
	        turbineDeployedId: '',
	        turbineId: '',
	        siteId: '',
	        serialNumber: '',
	        deployedDate: '',
	        totalFiredHours: '',
	        totalStarts: '',
        	lastPlannedOutageDate: '',
        	lastUnplannedOutageDate: ''
    	}
		],
		comments: [
    	{
        noteId: '',
        notes: '',
        clientId: ''
    	}
		],
			testLorem: 'Test Lorem Ipsum Dolor',
			plannedOutageDiff: '',
			unplannedOutageDiff: ''
		},
	  methods: {
			fetchOrders() {
				fetch('http://35.237.105.120/api/client_purchase_data.php')
	      .then(response => response.json())
	      .then (json => {
					cumminsApp.clientPurchaseDetails = json;
					// console.log(commentsApp.turbineDeployed);
					console.log(cumminsApp.clientPurchaseDetails)
				})
	      .catch( function(err){
	        console.log(err)
	      })
			},
			fetchServiceLog() {
				// reference: https://firebase.google.com/docs/reference/js/firebase.database.DataSnapshot#forEach
				const database = firebase.database();
				const ref = database.ref('serviceLog').orderByKey();
				ref.once('value')
					.then(function(snapshot){
						snapshot.forEach(function(childSnapshot) {
							// console.log(childSnapshot.val());
							cumminsApp.serviceLog.push(childSnapshot.val())
							console.log(cumminsApp.serviceLog)
						})
					})
			},
			fetchFaultLogs() {
				// reference: https://firebase.google.com/docs/reference/js/firebase.database.DataSnapshot#forEach
				const database = firebase.database();
				const ref = database.ref('faultLog').orderByKey();
				ref.once('value')
					.then(function(snapshot){
						snapshot.forEach(function(childSnapshot) {
							cumminsApp.faultLogNumber += 1;
							console.log(childSnapshot.val());
							cumminsApp.faultLogs.push(childSnapshot.val());
							if (childSnapshot.val().peakPressure > 1300) {
									cumminsApp.detectedProblems.push('peakPressure')
							}
							console.log(cumminsApp.detectedProblems)
						})
					})
					// cumminsApp.faultLogNumber = cumminsApp.faultLogs.length;
			},
			fetchEngineStats() {
				// reference: https://firebase.google.com/docs/reference/js/firebase.database.DataSnapshot#forEach
				const database = firebase.database();
				const ref = database.ref('engineTimeSeries').orderByKey();
				ref.once('value')
					.then(function(snapshot){
						snapshot.forEach(function(childSnapshot) {
							totalVolEff = 0;
							totalThermalEff = 0;
							totalPeakPressure = 0;
							totalFuelAir = 0;
							cumminsApp.engineTimeSeries.push(childSnapshot.val())
							// console.log(cumminsApp.engineTimeSeries)
							numInstances = cumminsApp.engineTimeSeries.length;
							for (var i=0; i < numInstances; i++) {
								totalVolEff = totalVolEff + cumminsApp.engineTimeSeries[i].volumetricEfficiency;
								totalThermalEff = totalThermalEff + cumminsApp.engineTimeSeries[i].thermalEfficiency;
								totalPeakPressure = totalPeakPressure + cumminsApp.engineTimeSeries[i].peakPressure;
								totalFuelAir = totalFuelAir + cumminsApp.engineTimeSeries[i].fuelAirRatio;
							}
							cumminsApp.avgVolEff = totalVolEff/numInstances;
							cumminsApp.avgFuelAir = totalFuelAir/numInstances;
							cumminsApp.avgThermalEff = totalThermalEff/numInstances;
							cumminsApp.avgPeakPressure = totalPeakPressure/numInstances;
							if (document.getElementById("batteryPlot")) {
								cumminsApp.buildBatteryPlot();
								cumminsApp.buildPeakPressurePlot();
								cumminsApp.buildVolEffPlot();
								cumminsApp.buildThermalEffPlot();
								cumminsApp.buildfuelAirRatioPlot();
							}

						})
					})
			},
			fetchClientDetails() {
				fetch('http://35.237.105.120/api/client_data.php')
	      .then(response => response.json())
	      .then (json => {
					cumminsApp.clientDetails = json;
					// console.log(commentsApp.turbineDeployed);
					console.log(cumminsApp.clientDetails)
				})
	      .catch( function(err){
	        console.log(err)
	      })
			},
			buildBatteryPlot() {
				Highcharts.chart('batteryPlot', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Battery Status: 1134'
            },
            yAxis: {
                title: {
                    text: 'Battery %'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 1,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[3]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [
							{
                type: 'line',
                name: 'Battery %',
								// data: [[1,55],[2,56],[3,67],[4,89]]
                data: this.engineTimeSeries.map( entry => [entry.timestamp, entry.batteryStatus])
            }]
        });
			},
			buildPeakPressurePlot() {
				Highcharts.chart('peakPressurePlot', {
						chart: {
								zoomType: 'x'
						},
						title: {
								text: 'Peak Pressure: 1134'
						},
						yAxis: {
								title: {
										text: 'Pressure %'
								}
						},
						legend: {
								enabled: false
						},
						plotOptions: {
							line: {
        dataLabels: {
            enabled: true
        }
    },
								area: {
										fillColor: {
												linearGradient: {
														x1: 1,
														y1: 0,
														x2: 0,
														y2: 1
												},
												stops: [
														[0, Highcharts.getOptions().colors[3]],
														[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
												]
										},
										marker: {
												radius: 2
										},
										lineWidth: 1,
										states: {
												hover: {
														lineWidth: 1
												}
										},
										threshold: null
								}
						},

						series: [{
								type: 'line',
								name: 'Regression Line',
								data: [[0, 700], [15, 1400]],
								marker: {
										enabled: true
								},
								states: {
										hover: {
												lineWidth: 0
										}
								},
								enableMouseTracking: false
						},
						{
								type: 'line',
								dashStyle: 'longdash',
								color: '#cccccc',
								data: [[0, 1300], [15, 1300]],
								marker: {
										enabled: true
								},
								states: {
										hover: {
												lineWidth: 0
										}
								},
								enableMouseTracking: false
						},
							{
								type: 'line',
								name: 'Pressure %',
								// data: [[1,55],[2,56],[3,67],[4,89]]
								data: this.engineTimeSeries.map( entry => [entry.timestamp, entry.peakPressure])
						}]
				});
			},
			buildVolEffPlot() {
				Highcharts.chart('volumetricEffPlot', {
						chart: {
								zoomType: 'x'
						},
						title: {
								text: 'Volumetric Efficiency: 1134'
						},
						yAxis: {
								title: {
										text: 'vEff %'
								}
						},
						legend: {
								enabled: false
						},
						plotOptions: {
								area: {
										fillColor: {
												linearGradient: {
														x1: 1,
														y1: 0,
														x2: 0,
														y2: 1
												},
												stops: [
														[0, Highcharts.getOptions().colors[1]],
														[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
												]
										},
										marker: {
												radius: 2
										},
										lineWidth: 1,
										states: {
												hover: {
														lineWidth: 1
												}
										},
										threshold: null
								}
						},

						series: [
							{
								type: 'area',
								name: 'vEff',
								// data: [[1,55],[2,56],[3,67],[4,89]]
								data: this.engineTimeSeries.map( entry => [entry.timestamp, entry.volumetricEfficiency])
						}]
				});
			},
			buildThermalEffPlot() {
				Highcharts.chart('thermalEffPlot', {
						chart: {
								zoomType: 'x'
						},
						title: {
								text: 'Thermal Efficiency: 1134'
						},
						yAxis: {
								title: {
										text: 'tEff %'
								}
						},
						legend: {
								enabled: false
						},
						plotOptions: {
								area: {
										fillColor: {
												linearGradient: {
														x1: 1,
														y1: 0,
														x2: 0,
														y2: 1
												},
												stops: [
														[0, Highcharts.getOptions().colors[2]],
														[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
												]
										},
										marker: {
												radius: 2
										},
										lineWidth: 1,
										states: {
												hover: {
														lineWidth: 1
												}
										},
										threshold: null
								}
						},

						series: [
							{
								type: 'area',
								name: 'tEff',
								// data: [[1,55],[2,56],[3,67],[4,89]]
								data: this.engineTimeSeries.map( entry => [entry.timestamp, entry.thermalEfficiency])
						}]
				});
			},
			buildfuelAirRatioPlot() {
				Highcharts.chart('fuelAirRatioPlot', {
						chart: {
								zoomType: 'x'
						},
						title: {
								text: 'Fuel Air Ratio: 1134'
						},
						yAxis: {
								title: {
										text: 'fuel/air'
								}
						},
						legend: {
								enabled: false
						},
						plotOptions: {
								area: {
										fillColor: {
												linearGradient: {
														x1: 1,
														y1: 0,
														x2: 0,
														y2: 1
												},
												stops: [
														[0, Highcharts.getOptions().colors[3]],
														[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
												]
										},
										marker: {
												radius: 2
										},
										lineWidth: 1,
										states: {
												hover: {
														lineWidth: 1
												}
										},
										threshold: null
								}
						},

						series: [
							{
								type: 'area',
								name: 'fuel/air',
								// data: [[1,55],[2,56],[3,67],[4,89]]
								data: this.engineTimeSeries.map( entry => [entry.timestamp, entry.fuelAirRatio])
						}]
				});
			},
			fetchCientInfo() {
				// console.log(document.getElementById("feedbackComment").value);
	      fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/client.php')
	      .then(response => response.json())
	      .then (json => {
					commentsApp.clientInfo = json;
					console.log(commentsApp.clientInfo);
				})
	      .catch( function(err){
	        console.log(err)
	      })
	    },
			fetchSitesInfo() {
				// console.log(document.getElementById("feedbackComment").value);
	      fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/site.php')
	      .then(response => response.json())
	      .then (json => {
					commentsApp.sites = json;
					console.log(commentsApp.sites);
				})
	      .catch( function(err){
	        console.log(err)
	      })
	    },
			fetchSensorTimeSeries() {
				// console.log(document.getElementById("feedbackComment").value);
	      fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/sensorTimeSeries.php')
	      .then(response => response.json())
	      .then (json => {
					commentsApp.sensorTimeSeries = json;
					this.formatSensorTime();
					if (document.getElementById("mainAvailabilityChart")) {
						this.buildAvailabilityChart();
					}
					this.buildSensorAvailability();
					this.buildSensorHeatRate();
					this.buildSensorReliability();
					this.buildSensorTrips();
					this.buildSensorStarts();
					this.buildSensorEfficiency();
					console.log("MAPPED DATA!:", this.sensorTimeSeries.map( entry => [entry.dateCollected, entry.availability]));
				})
	      .catch( function(err){
	        console.log(err)
	      })
	    },
			fetchAllTurbinesData() {
				fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/turbineDeployed.php')
	      .then(response => response.json())
	      .then (json => {
					commentsApp.turbineDeployed = json;
					console.log(commentsApp.turbineDeployed);
				})
	      .catch( function(err){
	        console.log(err)
	      })
			},
			fetchSpecificTurbinesData(siteId) {
				fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/turbineDeployed.php?turbineDeployedId='+siteId)
	      .then(response => response.json())
	      .then (json => {
					commentsApp.testLorem = "Changed lorem ipsum dolor";
					commentsApp.turbineDeployedSpecific = json;
					commentsApp.plannedOutageDiff = calcTimeDiff(commentsApp.turbineDeployedSpecific[0].lastPlannedOutageDate);
					commentsApp.unplannedOutageDiff = calcTimeDiff(commentsApp.turbineDeployedSpecific[0].lastUnplannedOutageDate)
					console.log(commentsApp.turbineDeployedSpecific);
				})
	      .catch( function(err){
	        console.log(err)
	      })
			},
			addClientComment() {
			  fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/clientNotes.php', {
			      body : JSON.stringify({

			          "clientId": document.getElementById("clientId").value,
			          "notes": document.getElementById("comment").value

			      }),
			      // mode: "no-cors", // no-cors, cors, *same-origin
			      headers: {
			        'Accept': 'application/json, text/plain, */*',
			        'Content-Type': 'application/json; charset=utf-8'
			      },
			      method: "POST"
			    }
			  )
			  .then(function(resp) {
			    console.log(resp.json());
			    // console.log(document.getElementById("comment").value);
			    // plotAvailability();
			  })
			  .catch( function(err){
			    console.log(err)
			  })
			},
			fetchClientComments() {
				fetch('http://ec2-13-233-94-247.ap-south-1.compute.amazonaws.com/api/clientNotes.php?clientId=2')
	      .then(response => response.json())
	      .then (json => {
					commentsApp.comments = json;
					console.log(commentsApp.comments);
				})
	      .catch( function(err){
	        console.log(err)
	      })
			},
			formatSensorTime(){
				this.sensorTimeSeries.forEach(
				(entry, index, arr) => {
					entry.dataCollectedDate = entry.dataCollectedDate;
					entry.output = Number(entry.output);
					entry.heatRate = Number(entry.heartRate);
					entry.compressorEfficiency = Number(entry.compressorEfficiency);
					entry.availability = Number(entry.availability);
					entry.reliability = Number(entry.reliability);
					entry.fixedHours = Number(entry.firedHours);
					entry.trips = Number(entry.trips);
					entry.starts = Number(entry.starts);
				})
			},
// 			buildAvailabilityChart(){
//         Highcharts.chart('mainAvailabilityChart', {
//             chart: {
//                 zoomType: 'x'
//             },
//             title: {
//                 text: 'Availability for sensor 2'
//             },
//             yAxis: {
//                 title: {
//                     text: 'Availability %'
//                 }
//             },
//             legend: {
//                 enabled: false
//             },
//             plotOptions: {
//                 area: {
//                     fillColor: {
//                         linearGradient: {
//                             x1: 1,
//                             y1: 0,
//                             x2: 0,
//                             y2: 1
//                         },
//                         stops: [
//                             [0, Highcharts.getOptions().colors[3]],
//                             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
//                         ]
//                     },
//                     marker: {
//                         radius: 2
//                     },
//                     lineWidth: 1,
//                     states: {
//                         hover: {
//                             lineWidth: 1
//                         }
//                     },
//                     threshold: null
//                 }
//             },
//
//             series: [{
//                 type: 'area',
//                 name: 'Availability %',
// 								// data: [[1,55],[2,56],[3,67],[4,89]]
//                 data: this.sensorTimeSeries.map( entry => [entry.dataCollectedDate, entry.availability])
//             }]
//         });
// 			},
// 			buildSensorAvailability(){
//         Highcharts.chart('sensorAvailabilityChart', {
//             chart: {
//                 zoomType: 'x'
//             },
//             title: {
//                 text: 'Availability for sensor 2'
//             },
//             yAxis: {
//                 title: {
//                     text: 'Availability %'
//                 }
//             },
//             legend: {
//                 enabled: false
//             },
//             plotOptions: {
//                 area: {
//                     fillColor: {
//                         linearGradient: {
//                             x1: 0,
//                             y1: 0,
//                             x2: 0,
//                             y2: 1
//                         },
//                         stops: [
//                             [0, Highcharts.getOptions().colors[0]],
//                             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
//                         ]
//                     },
//                     marker: {
//                         radius: 2
//                     },
//                     lineWidth: 1,
//                     states: {
//                         hover: {
//                             lineWidth: 1.3
//                         }
//                     },
//                     threshold: null
//                 }
//             },
//
//             series: [{
//                 type: 'area',
//                 name: 'Availability %',
// 								// data: [[1,55],[2,56],[3,67],[4,89]]
//                 data: this.sensorTimeSeries.map( entry => [entry.dataCollectedDate.toString(), entry.availability])
//             }]
//         });
// 			},
// 			buildSensorReliability(){
//         Highcharts.chart('sensorReliabilityChart', {
//     yAxis: {
//         min: 95,
// 				max: 100
//     },
//     title: {
//         text: 'Reliability'
//     },
//     series: [{
//         type: 'line',
//         name: 'Regression Line',
//         data: [[0, 98], [150, 98.5]],
//         marker: {
//             enabled: false
//         },
//         states: {
//             hover: {
//                 lineWidth: 0
//             }
//         },
//         enableMouseTracking: false
//     }, {
//         type: 'scatter',
//         name: 'Observations',
//         data: this.sensorTimeSeries.map( entry => entry.reliability),
//         marker: {
//             radius: 4
//         }
//     }]
// });
// 			},
// 			buildSensorEfficiency(){
//         Highcharts.chart('sensorEfficiencyChart', {
//             chart: {
//                 zoomType: 'x'
//             },
//             title: {
//                 text: 'Compressor Efficiency for sensor 2'
//             },
//             yAxis: {
//                 title: {
//                     text: 'Availability %'
//                 }
//             },
//             legend: {
//                 enabled: false
//             },
//             plotOptions: {
//                 area: {
//                     fillColor: {
//                         linearGradient: {
//                             x1: 0,
//                             y1: 0,
//                             x2: 0,
//                             y2: 1
//                         },
//                         stops: [
//                             [0, Highcharts.getOptions().colors[0]],
//                             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
//                         ]
//                     },
//                     marker: {
//                         radius: 2
//                     },
//                     lineWidth: 1,
//                     states: {
//                         hover: {
//                             lineWidth: 1.3
//                         }
//                     },
//                     threshold: null
//                 }
//             },
//
//             series: [{
//                 type: 'area',
//                 name: 'Availability %',
// 								// data: [[1,55],[2,56],[3,67],[4,89]]
//                 data: this.sensorTimeSeries.map( entry => [entry.dataCollectedDate.toString(), entry.compressorEfficiency])
//             }]
//         });
// 			},
// 			buildSensorStarts(){
//         Highcharts.chart('sensorStartsChart', {
//             chart: {
//                 zoomType: 'x'
//             },
//             title: {
//                 text: 'Starts for sensor 2'
//             },
//             yAxis: {
//                 title: {
//                     text: 'Availability %'
//                 }
//             },
//             legend: {
//                 enabled: false
//             },
//             plotOptions: {
//                 area: {
//                     fillColor: {
//                         linearGradient: {
//                             x1: 0,
//                             y1: 0,
//                             x2: 0,
//                             y2: 1
//                         },
//                         stops: [
//                             [0, Highcharts.getOptions().colors[0]],
//                             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
//                         ]
//                     },
//                     marker: {
//                         radius: 2
//                     },
//                     lineWidth: 1,
//                     states: {
//                         hover: {
//                             lineWidth: 1.4
//                         }
//                     },
//                     threshold: null
//                 }
//             },
//
//             series: [{
//                 type: 'area',
//                 name: 'Availability %',
//                 data: this.sensorTimeSeries.map( entry => [entry.dataCollectedDate.toString(), entry.starts])
//             }]
//         });
// 			},
// 			buildSensorTrips(){
//         Highcharts.chart('sensorTripsChart', {
// 				    chart: {
// 				        type: 'scatter',
// 				        zoomType: 'xy'
// 				    },
// 				    title: {
// 				        text: 'Starts and Trips for sensor 2'
// 				    },
// 				    xAxis: {
// 				        title: {
// 				            enabled: true,
// 				            text: 'Date'
// 				        },
// 				        startOnTick: true,
// 				        endOnTick: true,
// 				        showLastLabel: true
// 				    },
// 				    yAxis: {
// 				        title: {
// 				            text: 'Number'
// 				        }
// 				    },
// 				    legend: {
// 				        layout: 'vertical',
// 				        align: 'left',
// 				        verticalAlign: 'top',
// 				        x: 100,
// 				        y: 70,
// 				        floating: true,
// 				        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
// 				        borderWidth: 1
// 				    },
// 				    plotOptions: {
// 				        scatter: {
// 				            marker: {
// 				                radius: 5,
// 				                states: {
// 				                    hover: {
// 				                        enabled: true,
// 				                        lineColor: 'rgb(100,100,100)'
// 				                    }
// 				                }
// 				            },
// 				            states: {
// 				                hover: {
// 				                    marker: {
// 				                        enabled: false
// 				                    }
// 				                }
// 				            },
// 				            tooltip: {
// 				                headerFormat: '<b>{series.name}</b><br>',
// 				                pointFormat: '{point.x} had {point.y} {series.name} '
// 				            }
// 				        }
// 				    },
// 				    series: [{
// 				        name: 'Starts',
// 				        color: 'rgba(223, 83, 83, .5)',
// 				        data:  this.sensorTimeSeries.map( entry => [entry.dataCollectedDate.toString(), entry.starts]) }, {
// 				        name: 'Trips',
// 				        color: 'rgba(119, 152, 191, .5)',
// 				        data: this.sensorTimeSeries.map( entry => [entry.dataCollectedDate.toString(), entry.trips])
// 				    }]
// 				});
// 			},
// 			buildSensorHeatRate(){
//         Highcharts.chart('sensorHeatRateChart', {
// 				    chart: {
// 				        type: 'column'
// 				    },
// 				    title: {
// 				        text: 'HeatRate'
// 				    },
// 				    xAxis: {
// 				        type: 'category',
// 				        labels: {
// 				            rotation: -90,
// 				            style: {
// 				                fontSize: '8px',
// 				                fontFamily: 'Verdana, sans-serif'
// 				            }
// 				        }
// 				    },
// 				    yAxis: {
// 				        min: 0,
// 				        title: {
// 				            text: 'HeatRate'
// 				        }
// 				    },
// 				    legend: {
// 				        enabled: false
// 				    },
// 				    tooltip: {
// 				        pointFormat: 'HeatRate: <b>{point.y:.1f}</b>'
// 				    },
// 				    series: [{
// 				        name: 'sensorHeatRate',
// 				        data: this.sensorTimeSeries.map( entry => [entry.dataCollectedDate.toString(), entry.heatRate]),
// 				        dataLabels: {
// 				            enabled: true,
// 				            rotation: -90,
// 				            color: '#FFFFFF',
// 				            align: 'right',
// 				            format: '{point.y:.1f}', // one decimal
// 				            y: 10, // 10 pixels down from the top
// 				            style: {
// 				                fontSize: '6px',
// 				                fontFamily: 'Verdana, sans-serif'
// 				            }
// 				        }
// 				    }]
// 				});
// 			},
			mapper(lats, lngs, pp) {
					markerUrls = [];
					// console.log(pp);
					for (var i=0; i<lats.length; i++) {
						if (pp[i] > 1300) {
							markerUrls.push('images/red2.png')
						}
						else {
							markerUrls.push('images/green2.png')
						}
					}
					var location = new google.maps.LatLng(lats[0],lngs[0]);
					var markerFileUrl = "js/Ripple.svg";
	        // var testLocation = {lat: 17.74234, lng: 45.2378};
					var mapProp= {
					    center: location,
					    zoom:15,
							mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					var icon = {
						url: markerFileUrl, // url
						scaledSize: new google.maps.Size(200, 200), // scaled size
						origin: new google.maps.Point(0,0), // origin
						anchor: new google.maps.Point(100,100) // anchor
					};

					var infoWindow = new google.maps.InfoWindow();

					var map = new google.maps.Map(document.getElementById("pathTracker"),mapProp);

					for (var i=0; i<lats.length; i++) {
						var marker = new google.maps.Marker({
							position: new google.maps.LatLng(lats[i], lngs[i]),
							// icon: 'images/marker2.png',
							animation: google.maps.Animation.DROP,
							icon: markerUrls[i],
							map: map
						});
						google.maps.event.addListener(marker, 'click', (function(marker, i){
							return function(){
								// infoWindow.setContent("<a href=\"https://www.google.com/search?q="+memoryPlaces[i]+"\" target=\"_blank\">"+memoryPlaces[i]+"</a>");
								infoWindow.setContent("<b>Oil</b>: "+cumminsApp.engineTimeSeries[i].oilStatus+"<br><b>Battery</b>: "+cumminsApp.engineTimeSeries[i].batteryStatus+"<br><b>Fuel/Air</b>: "+cumminsApp.engineTimeSeries[i].fuelAirRatio+"<br><b>Pressure</b>: "+cumminsApp.engineTimeSeries[i].peakPressure+"<br><b>Vol. Eff.</b>: "+cumminsApp.engineTimeSeries[i].volumetricEfficiency+"<br><b>Th. Eff.</b>: "+cumminsApp.engineTimeSeries[i].thermalEfficiency);
								infoWindow.open(map, marker);
							}
						})(marker, i));
					}
			},
			getAllLatLngs() {
				var lats = [];
				var lngs = [];
				var peakPressures = [];
				const database = firebase.database();
				const ref = database.ref('engineTimeSeries').orderByKey();
				ref.once('value')
					.then(function(snapshot){
						snapshot.forEach(function(childSnapshot) {
							// console.log(childSnapshot.val());
							lats.push(childSnapshot.val().lat);
							lngs.push(childSnapshot.val().lng);
							peakPressures.push(childSnapshot.val().peakPressure)
						})
						cumminsApp.mapper(lats, lngs, peakPressures);
					})
					//cumminsApp.mapper();
			}
	  },
		created() {
			this.fetchServiceLog();
			this.fetchEngineStats();
			this.fetchFaultLogs();
			this.fetchClientDetails();
			this.fetchOrders();
			this.getAllLatLngs();
			// this.mapper();
		}
	})

	function calcTimeDiff(date){
		given = moment(date, "YYYY-MM-DD");
		current = moment().startOf('day');
		diff = moment.duration(given.diff(current)).asDays();
		return diff+" days ago";
	}

	function hideFelix() {
		var x = document.getElementById("felix-hidden");
		if (x.style.display === "none") {
			x.style.display = "block";
		}
		else {
			x.style.display = "none";
		}
	}
