function buildBatteryPlot() {
  Highcharts.chart('batteryPlot', {
      chart: {
          zoomType: 'x'
      },
      title: {
          text: 'Battery Status: 1134'
      },
      yAxis: {
          title: {
              text: 'Battery %'
          }
      },
      legend: {
          enabled: false
      },
      plotOptions: {
          area: {
              fillColor: {
                  linearGradient: {
                      x1: 1,
                      y1: 0,
                      x2: 0,
                      y2: 1
                  },
                  stops: [
                      [0, Highcharts.getOptions().colors[3]],
                      [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                  ]
              },
              marker: {
                  radius: 2
              },
              lineWidth: 1,
              states: {
                  hover: {
                      lineWidth: 1
                  }
              },
              threshold: null
          }
      },

      series: [{
          type: 'line',
          name: 'Battery %',
          // data: [[1,55],[2,56],[3,67],[4,89]]
          data: this.engineTimeSeries.map( entry => [entry.timestamp, entry.batteryStatus])
      }]
  });
}
