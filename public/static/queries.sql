DROP TABLE CLIENT_DATA;

CREATE TABLE CLIENT_DATA (
	clientID INT PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	addressCity VARCHAR(50) NOT NULL,
	addressState VARCHAR(50) NOT NULL,
	phone VARCHAR(20) NOT NULL,
	yearsSinceClient INT NOT NULL,
	lastPurchaseID INT
)

CREATE TABLE SUPPLIER_DATA (
	supplierID INT PRIMARY KEY,
	supplierName VARCHAR(50) NOT NULL,
	addressCity VARCHAR(50) NOT NULL,
	addressState VARCHAR(50) NOT NULL,
	phone VARCHAR(20) NOT NULL
)

CREATE TABLE CLIENT_PURCHASE_DATA (
	purchaseID INT PRIMARY KEY,
	clientID INT NOT NULL,
	dateOfPurchase VARCHAR(20) NOT NULL,
	productOrdered VARCHAR(50) NOT NULL,
	quantity INT NOT NULL,
	totalAmount INT NOT NULL,
	status VARCHAR(20) NOT NULL
)

CREATE TABLE ENGINES (
	engineID INT PRIMARY KEY,
	engineName VARCHAR(70) NOT NULL,
	capacity INT NOT NULL,
	numOfParts INT NOT NULL,
	partIDs VARCHAR(80) NOT NULL,
	retailPrice INT NOT NULL
)


CREATE TABLE PARTS_DATA (
	partID VARCHAR(10) PRIMARY KEY,
	description VARCHAR(80) NOT NULL,
	purchasePrice INT NOT NULL
)
-- purchaseData - purchaseID, clientID, dateOfPurchase, description, totalAmount, status

-- TypesOfEngines - engineID, name, capacity, numOfParts, partIDs, retailPrice

-- partsData - partID, description, purchasePrice

INSERT INTO CLIENT_DATA(clientID, name, addressCity, addressState, phone, yearsSinceClient, lastPurchaseID) VALUES (111, 'Aecom', 'LA', 'California', '812361321', 10, 121)
INSERT INTO CLIENT_DATA(clientID, name, addressCity, addressState, phone, yearsSinceClient, lastPurchaseID) VALUES (112, 'Hudson Tech.', 'Pearl River', 'New York', '8326549871', 6, 122);
INSERT INTO CLIENT_DATA(clientID, name, addressCity, addressState, phone, yearsSinceClient, lastPurchaseID) VALUES (113, 'Tesla Inc.', 'San Carlos', 'California', '8127629871', 3, 123);

INSERT INTO SUPPLIER_DATA(supplierID, supplierName, addressCity, addressState, phone) VALUES (131, 'Teraworks', 'NY City', 'New York', '1648934572');

INSERT INTO CLIENT_PURCHASE_DATA(purchaseID, clientID, dateOfPurchase, productOrdered, quantity, totalAmount, status) VALUES (121, 111, '12-03-2018', 'QSX11.9 (tier 4 interim)', 300, 150000, 'in production');
INSERT INTO CLIENT_PURCHASE_DATA(purchaseID, clientID, dateOfPurchase, productOrdered, quantity, totalAmount, status) VALUES (122, 112, '12-18-2018', 'QSX15 (Tier 4 Final/Stage IV)', 350, 130000, 'under quality check');
INSERT INTO CLIENT_PURCHASE_DATA(purchaseID, clientID, dateOfPurchase, productOrdered, quantity, totalAmount, status) VALUES (123, 113, '11-12-2018', 'ISX15 (EPA 2010)', 380, 190000, 'shipped');

INSERT INTO ENGINES(engineID, engineName, capacity, numOfParts, partIDs, retailPrice) VALUES (141, 'Cummins 5.0L V8 Turbo Diesel', '5.0 L', 300, 'p1,p2,p3,p4,p5', 500);
INSERT INTO ENGINES(engineID, engineName, capacity, numOfParts, partIDs, retailPrice) VALUES (142, 'Cummins 6.7L Turbo Diesel', '6.7 L', 350, 'p1,p2,p6,p7,p9', 400);

INSERT INTO PARTS_DATA(partID, description, purchasePrice) VALUES ('p1', '3-inch valve', 40);
INSERT INTO PARTS_DATA(partID, description, purchasePrice) VALUES ('p2', '5-inch valve', 42);
INSERT INTO PARTS_DATA(partID, description, purchasePrice) VALUES ('p3', '4mm micro cylinder', 10);
